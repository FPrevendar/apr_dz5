## 5\. domaća zadaća iz analize i projektiranja računalom

U ovoj domaćoj zadaći je bilo potrebno ostvarititi nekoliko postupaka numeričke integracije:
* Eulerov postupak
* Obrnuti Eulerov postupak
* Trapezni postupak
* Runge-Kutta postupak 4. reda
* Prediktorsko-korektorski postupak (PECE)

To je ostvareno u programskom jeziku java, i koriste se metode ostvarene u [1\.](https://gitlab.com/FPrevendar/apr_dz1), [2\.](https://gitlab.com/FPrevendar/apr_dz2), [3\.](https://gitlab.com/FPrevendar/apr_dz3) i [4\.](https://gitlab.com/FPrevendar/apr_dz4) domaćoj zadaći.

Tekst zadatka se nalazi [ovdje](https://www.fer.unizg.hr/_download/repository/Zadatak_za_5._zadacu.pdf).
