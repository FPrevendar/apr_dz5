import matplotlib.pyplot as plt

imenaDatoteka = ["PrviZadatak.txt", "DrugiZadatak.txt", "TreciZadatak.txt", "CetvrtiZadatak.txt"]
for imeDatoteke in imenaDatoteka:
    with open(imeDatoteke) as file_in:
        oznakaPerioda = "T "
        oznakatmax = "tmax"
        oznakaX0 = "X0"
        oznakaDimenzije = "dim = "
        oznakax0 = "X0 = "
        dimenzija = 0
        postupci = []
        trenutniPostupak = None
        podaci = []
        trenutniPodaci = []
        period = None
        tmax = None
        x0 = []
        for linija in file_in:
            #print(linija)
            if linija.startswith(oznakaPerioda):
                period = float(linija.strip().split(" ")[1])
            elif linija.startswith(oznakatmax):
                tmax = float(linija.strip().split(" ")[1])
            elif linija.startswith(oznakaDimenzije):
                dimenzija = int(linija.strip().split(" ")[2])
            elif linija.startswith(oznakax0):
                x0 = [float(i) for i in linija.strip().split(" ")[2:]]
            elif trenutniPostupak == None:
                trenutniPostupak = linija.strip()
                postupci.append(trenutniPostupak)
            else:
                if len(linija.strip().split(" ")) != dimenzija:
                    trenutniPostupak = linija.strip()
                    postupci.append(trenutniPostupak)
                    podaci.append(trenutniPodaci)
                    trenutniPodaci = []
                else:
                    try:
                        red = [float(i) for i in linija.strip().split(" ")]
                        trenutniPodaci.append(red)
                    except:
                        trenutniPostupak = linija.strip()
                        postupci.append(trenutniPostupak)
                        podaci.append(trenutniPodaci)
                        trenutniPodaci = []
        podaci.append(trenutniPodaci)
        vremena = [0]
        t = period
        while len(vremena) != len(podaci[0]) + 1:
            vremena.append(t)
            t += period
        plt.suptitle(imeDatoteke)
        for i in range(dimenzija):
            plt.subplot(dimenzija, 1, i + 1)
            for j in range(len(podaci)):
                plt.plot(vremena, [x0[i]] + [x[i] for x in podaci[j]], label = postupci[j])
                plt.xlabel("t")
                plt.ylabel("x" + str(i + 1))
            plt.legend()
        plt.savefig(imeDatoteke[:-4] + ".pdf",  bbox_inches='tight')
        plt.show()
