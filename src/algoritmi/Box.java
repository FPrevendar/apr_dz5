package algoritmi;

import java.util.Random;

import funkcije.IFunction;
import ogranicenja.EksplicitnaOgranicenjaZaSveVarijable;
import ogranicenja.OgranicenjeNejednakosti;
import point.IPoint;
import point.Point;

public class Box {
	
	private static final int MAX_DO_DIVERGENCIJE = 100;
	
	public static IPoint BoxovPostupak(IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			EksplicitnaOgranicenjaZaSveVarijable eksplicitna,
			OgranicenjeNejednakosti[] nejednakosti,
			double alfa,
			double preciznost) {
		
		if(!eksplicitna.jeLiZadovoljeno(pocetnaTocka)) {
			throw new IllegalArgumentException("Pocetna tocka ne postuje eksplicitna ogranicenja");
		}
		if(!jesuZadovoljeneNejednakosti(nejednakosti, pocetnaTocka)) {
			throw new IllegalArgumentException("Pocetna tocka ne postuje implicitna ogranicenja");
		}
		IPoint xd = eksplicitna.getMin();
		IPoint xg = eksplicitna.getMax();
		Random rand = new Random();
		IPoint xc = pocetnaTocka.copy();
		IPoint[] tocke = new IPoint[2 * xc.getDimenstion()];
		double[] vrijednosti = new double[2 * xc.getDimenstion()];
		tocke[0] = xc;
		vrijednosti[0] = funkcijaCilja.getValueAtPoint(xc);
		double najboljaVrijednost = vrijednosti[0];
		int najboljaTocka = 0;
		int provjeraDivergencije = 0;
		for(int t = 1; t < 2 * xc.getDimenstion(); t++) {
			IPoint novaTocka = new Point(xc.getDimenstion());
			for(int i = 0; i < xc.getDimenstion(); i++) {
				double r = rand.nextDouble();
				novaTocka.set(i, xd.get(i) + r*(xg.get(i) - xd.get(i)));	
			}
			while(!jesuZadovoljeneNejednakosti(nejednakosti, novaTocka)) {
				novaTocka = novaTocka.add(xc).mul(0.5);
			}
			tocke[t] = novaTocka;
			double v = funkcijaCilja.getValueAtPoint(novaTocka);
			vrijednosti[t] = v;
			if(najboljaVrijednost > v) {
				najboljaVrijednost = v;
				najboljaTocka = t;
			}
			xc = centroid(tocke);
		}
		double fxc = funkcijaCilja.getValueAtPoint(xc);
		
		/*
		System.out.println("Pocetni");
		for(IPoint p: tocke) {
			p.print();
		}*/
		
		do {
			int[] indeksi = odrediIndekse(vrijednosti);
			int h = indeksi[0];
			int h2 = indeksi[1];
			
			xc = centroidBezH(tocke, h);
			
			IPoint xr = xc.mul(1 + alfa).sub(tocke[h].mul(alfa));
			
			for(int i = 0; i < xr.getDimenstion(); i++) {
				if(xr.get(i) < xd.get(i)) {
					xr.set(i, xd.get(i));
				}else if(xr.get(i) > xg.get(i)) {
					xr.set(i, xg.get(i));
				}
			}
			
			while(!jesuZadovoljeneNejednakosti(nejednakosti, xr)) {
				xr = xr.add(xc).mul(0.5);
			}
			
			double fxr = funkcijaCilja.getValueAtPoint(xr);
			if(fxr > vrijednosti[h2]) {
				xr = xr.add(xc).mul(0.5);
				fxr = funkcijaCilja.getValueAtPoint(xr);
			}
			
			vrijednosti[h] = fxr;
			tocke[h] = xr;
			
			if(fxr < najboljaVrijednost) {
				najboljaVrijednost = fxr;
				provjeraDivergencije = 0;
				najboljaTocka = h;
			}else {
				provjeraDivergencije++;
			}
			/*
			System.out.println();
			System.out.println("Tocke");
			for(IPoint p: tocke) {
				p.print();
			}*/
			
		}while(!uvjetZaustavljanja(preciznost, fxc, vrijednosti) && provjeraDivergencije < MAX_DO_DIVERGENCIJE);
		
		
		return tocke[najboljaTocka];
	}
	
	private static boolean jesuZadovoljeneNejednakosti(
			OgranicenjeNejednakosti[] nejednakosti,
			IPoint tocka) {
		
		for(OgranicenjeNejednakosti nejed: nejednakosti) {
			if(!nejed.jeLiZadovoljeno(tocka)) {
				return false;
			}
		}
		return true;
	}
	
	private static IPoint centroid(IPoint[] tocke) {
		if(tocke[0] == null) {
			return null;
		}
		IPoint rez = new Point(tocke[0].getDimenstion());
		int broj = 0;
		for(IPoint tocka : tocke) {
			if(tocka != null) {
				rez = rez.add(tocka);
				broj++;
			}
		}
		return rez.mul((double)1/broj);
	}
	
	private static IPoint centroidBezH(IPoint[] tocke, int h) {
		IPoint rez = new Point(tocke[0].getDimenstion());
		int broj = 0;
		for(int i = 0; i < tocke.length; i++) {
			if(i != h) {
				rez = rez.add(tocke[i]);
				broj++;
			}
		}
		return rez.mul((double)1/broj);
	}
	
	private static boolean uvjetZaustavljanja(double epsilon, double fxc, double[] vrijednosti) {
		//return true;
		//TODO
		
		double sum = 0;
		for(double vrijednost: vrijednosti) {
			sum += Math.pow(fxc - vrijednost, 2);
		}
		sum /= vrijednosti.length;
		sum = Math.sqrt(sum);
		return sum < epsilon;
		
	}
	
	private static int[] odrediIndekse(double[] vrijednosti) {
		int[] indeksi = {0,0};
		double[] max = new double[2];
		max[0] = vrijednosti[0];
		max[1] = vrijednosti[1];
		
		for(int i = 0; i < vrijednosti.length; i++) {
			if(vrijednosti[i] > max[0]) {
				max[1] = max[0];
				indeksi[1] = indeksi[0];
				max[0] = vrijednosti[i];
				indeksi[0] = i;
			}
		}
		return indeksi;
	}
	
	public static IPoint BoxovPostupak(IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			EksplicitnaOgranicenjaZaSveVarijable eksplicitna,
			OgranicenjeNejednakosti[] nejednakosti) {
		return BoxovPostupak(funkcijaCilja,
				pocetnaTocka,
				eksplicitna,
				nejednakosti,
				1.3,
				10e-6);
	}

}
