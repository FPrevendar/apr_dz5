package algoritmi;

import funkcije.IFunction;
import point.IPoint;
import point.Point;

public class HookeJeeves{
	
	private static boolean ispis = false;
	
	public static void setIspis(boolean i) {
		ispis = false;
	}
	
	public static IPoint HookeJeevesPostupak(IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			IPoint preciznost,
			IPoint vektorPomaka) {
		
		IPoint dx = vektorPomaka.copy();
		IPoint x0 = pocetnaTocka.copy();
		IPoint xb = x0.copy();
		IPoint xp = x0.copy();
		do {
			IPoint xn = istrazi(funkcijaCilja, xp, dx);
			double fxn = funkcijaCilja.getValueAtPoint(xn);
			double fxb = funkcijaCilja.getValueAtPoint(xb);
			if(ispis) {
				System.out.println("xb");
				xb.print();
				System.out.println("f(xb) = " + fxb);
				System.out.println("xp");
				xp.print();
				System.out.println("f(xp) = " + funkcijaCilja.getValueAtPoint(xp));
				System.out.println("xn");
				xn.print();
				System.out.println("f(xn) = " + fxn);
				xn.print();
				System.out.println();
			}
			if(fxn < fxb) {
				xp = xn.mul(2).sub(xb);
				xb = xn;
			}else {
				dx = dx.mul(0.5); //smanji za pola
				xp = xb;
			}
		}while(!uvjetZaustavljanja(dx, preciznost));
		return xb;
	}
	
	private static IPoint istrazi(
			IFunction funkcijaCilja,
			IPoint xp,
			IPoint dx) {
		
		IPoint x = xp.copy();
		for(int i = 0; i < x.getDimenstion(); i++) {
			double p = funkcijaCilja.getValueAtPoint(x);
			x.set(i, x.get(i) + dx.get(i));
			double n = funkcijaCilja.getValueAtPoint(x);
			if(n > p) {
				x.set(i, x.get(i) - 2 * dx.get(i));
				n = funkcijaCilja.getValueAtPoint(x);
				if(n > p) {
					x.set(i, x.get(i) + dx.get(i));
				}
			}
		}
		return x;
	}
	
	private static boolean uvjetZaustavljanja(IPoint dx, IPoint preciznost) {
		for(int i = 0; i < dx.getDimenstion(); i++) {
			if(dx.get(i) < preciznost.get(i)) {
				return true;
			}
		}
		return false;
	}
	
	public static IPoint HookeJeevesPostupak(IFunction funkcijaCilja, IPoint pocetnaTocka) {
		IPoint vektorPomaka = Point.i(pocetnaTocka.getDimenstion()).mul(0.5);
		IPoint preciznost = Point.i(pocetnaTocka.getDimenstion()).mul(10e-6);
		return HookeJeevesPostupak(funkcijaCilja, pocetnaTocka, preciznost, vektorPomaka);
	}

}
