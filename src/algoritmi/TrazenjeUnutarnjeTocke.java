package algoritmi;

import algoritmi.TransformacijaUProblemBezOgranicenja.PostupakZaPrimjenu;
import funkcije.AbstractFunction;
import funkcije.IFunction;
import ogranicenja.OgranicenjeNejednakosti;
import point.IPoint;

public class TrazenjeUnutarnjeTocke {
	
	public static IPoint postupakTrazenjaUnutarnjeTocke(
			IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			OgranicenjeNejednakosti[] nejednakosti,
			PostupakZaPrimjenu postupak) {
		
		IFunction funkcija = new FunkcijaG(nejednakosti, pocetnaTocka.getDimenstion());
		
		IPoint tocka;
		
		if(postupak == PostupakZaPrimjenu.HOOKE_JEEVES) {
			tocka = HookeJeeves.HookeJeevesPostupak(funkcija, pocetnaTocka);
		}else{
			tocka = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(funkcija, pocetnaTocka);
		}
		return tocka;
	}
	
	private static class FunkcijaG extends AbstractFunction{
		
		private int dimenzija;
		private OgranicenjeNejednakosti[] ogr;
		
		public FunkcijaG(OgranicenjeNejednakosti[] ogr, int dimenzija) {
			this.dimenzija = dimenzija;
			this.ogr = ogr;
		}

		@Override
		protected double getValue(IPoint point) {
			double vrijednost = 0;
			for(OgranicenjeNejednakosti og : ogr) {
				if(og.vrijednostOgranicenja(point) < 0) {
					vrijednost -= og.vrijednostOgranicenja(point);
				}
			}
			return vrijednost;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == dimenzija) {
				return true;
			}
			return false;
		}
		
	}

}
