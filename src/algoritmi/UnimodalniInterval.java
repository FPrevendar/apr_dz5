package algoritmi;

import funkcije.IFunction;
import point.IPoint;

public class UnimodalniInterval {
	
	/**
	 * Metoda za pronalazenje unimodalnog intervala
	 * @param funkcijaCilja
	 * @param pocetnaTocka IMatrix vektor stupac koji predstavlja pocetnu tocku pretrazivanja
	 * @param pomakPretrazivanja
	 * @param koordinata redni broj koordinate po kojoj se trazi unimodalni interval, broji se od 0
	 * @return staticna klasa Interval koja sadrzi lijevi i desni rub intervala (samo trazena koordinata)
	 */
	public static Interval NadjiUnimodalniInterval(IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			double pomakPretrazivanja,
			int koordinata) {
		if(koordinata >= pocetnaTocka.getDimenstion()) {
			throw new IllegalArgumentException("Trazi se unimodalni interval po nepostojecoj koordinati");
		}
		IPoint tocka = pocetnaTocka.copy();
		Interval interval = new Interval();
		double m = tocka.get(koordinata);
		double toc = m;
		double l = m - pomakPretrazivanja;
		double r = m + pomakPretrazivanja;
		double korak = 1;
		double fm = funkcijaCilja.getValueAtPoint(tocka);
		tocka.set(koordinata, l);
		double fl = funkcijaCilja.getValueAtPoint(tocka);
		tocka.set(koordinata, r);
		double fr = funkcijaCilja.getValueAtPoint(tocka);
		if(fm < fr && fm < fl) {
			//propadni do dna
		}else if(fm > fr) {
			do {
				l = m;
				m = r;
				fm = fr;
				r = toc + pomakPretrazivanja * ( korak *= 2);
				tocka.set(koordinata, r);
				fr = funkcijaCilja.getValueAtPoint(tocka);
			}while(fm > fr);
		}else {
			do {
				r = m;
				m = l;
				fm = fl;
				l = toc - pomakPretrazivanja * (korak *= 2);
				tocka.set(koordinata, l);
				fl = funkcijaCilja.getValueAtPoint(tocka);
			}while(fm > fl);
		}
		interval.left = l;
		interval.right = r;
		interval.fl = fl;
		interval.fr = fr;
		interval.tocka  = pocetnaTocka.copy();
		interval.koordinata = koordinata;
		return interval;
	}
	
	public static Interval NadjiUnimodalniInterval(IFunction funkcijaCilja,	IPoint pocetnaTocka) {
		return NadjiUnimodalniInterval(funkcijaCilja, pocetnaTocka, 1, 0);
	}
	
}
