package funkcije;

import point.IPoint;

public class FunkcijaBroj3 extends AbstractFunction {
	
	private int dimenzija;
	
	

	public FunkcijaBroj3(int dimenzija) {
		this.dimenzija = dimenzija;
	}

	@Override
	protected double getValue(IPoint point) {
		double fx = 0;
		for(int i = 1; i <= dimenzija; i++) {
			fx += Math.pow(point.get(i - 1) - i, 2);
		}
		return fx;
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == dimenzija) {
			return true;
		}
		return false;
	}

}
