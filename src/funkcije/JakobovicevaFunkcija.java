package funkcije;

import point.IPoint;

public class JakobovicevaFunkcija extends AbstractFunction {

	@Override
	protected double getValue(IPoint point) {
		double x1 = point.get(0);
		double x2 = point.get(1);
		double fx = Math.abs((x1 -x2) * (x1 + x2)) + Math.sqrt(x1 * x1 + x2 * x2);
		return fx;
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == 2) {
			return true;
		}
		return false;
	}

}
