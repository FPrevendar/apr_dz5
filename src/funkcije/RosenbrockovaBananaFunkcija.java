package funkcije;

import point.IPoint;

public class RosenbrockovaBananaFunkcija extends AbstractFunction {

	@Override
	protected double getValue(IPoint point) {
		double x1 = point.get(0);
		double x2 = point.get(1);
		double fx = 100 * Math.pow(x2 - x1 * x1, 2) + Math.pow(1 - x1, 2);
		return fx;
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == 2) {
			return true;
		}
		return false;
	}

}
