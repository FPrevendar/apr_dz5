package funkcije;

import point.IPoint;

public class SchafferovaFunkcijaF6 extends AbstractFunction {
	
	private int dimenzija;

	public SchafferovaFunkcijaF6(int dimenzija) {
		this.dimenzija = dimenzija;
	}

	@Override
	protected double getValue(IPoint point) {
		double sumaxiKvadrat = 0;
		for(int i = 0; i < dimenzija; i++) {
			sumaxiKvadrat += Math.pow(point.get(i), 2);
		}
		double fx = 0.5 + (Math.pow(Math.sin(sumaxiKvadrat), 2) - 0.5)/
							Math.pow(1 + 0.001 * sumaxiKvadrat, 2);
		return fx;
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == dimenzija) {
			return true;
		}
		return false;
	}

}
