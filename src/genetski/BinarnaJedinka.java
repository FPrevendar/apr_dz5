package genetski;

import java.util.BitSet;
import java.util.Random;

import funkcije.IFunction;
import point.IPoint;
import point.Point;

public class BinarnaJedinka implements IJedinka{
	
	private BinarnaJedinkaBlueprint blueprint;
	private BitSet bitovi;
	private int dimenzija;
	
	public BinarnaJedinka(IPoint p, BinarnaJedinkaBlueprint blueprint) {
		if(p.getDimenstion() != blueprint.getDimenzija()) {
			throw new IllegalArgumentException("Kriva dimenzija");
		}
		this.blueprint = blueprint;
		this.dimenzija = p.getDimenstion();
		bitovi = new BitSet(blueprint.getDuljina());
		int[] offseti = blueprint.getOffset();
		int[] brojBitova = blueprint.getBrojBitovaPoDimenziji();
		for(int i = 0; i < dimenzija; i++) {
			int b = (int) Math.round((Math.pow(2, brojBitova[i]) - 1)*(p.get(i) - blueprint.getMin().get(i))/blueprint.getVelicinaIntervala().get(i));
			for(int j = 0; j < brojBitova[i]; j++) {
				if(b - (int)Math.pow(2, brojBitova[i] - j - 1) >= 0) {
					b -= (int)Math.pow(2, brojBitova[i] - j - 1);
					bitovi.set(offseti[i] + j);
				}
			}
		}
	}
	
	public BinarnaJedinka(BitSet set, BinarnaJedinkaBlueprint blueprint) {
		this.bitovi = set;
		this.blueprint = blueprint;
		this.dimenzija = blueprint.getDimenzija();
	}

	@Override
	public IPoint dekodiraj() {
		IPoint p = new Point(dimenzija);
		int[] offseti = blueprint.getOffset();
		int[] brojBitova = blueprint.getBrojBitovaPoDimenziji();
		for(int i = 0; i < dimenzija; i++) {
			int broj = 0;
			for(int j =0; j < brojBitova[i]; j++) {
				broj = broj * 2 + (bitovi.get(offseti[i] + j) ? 1 : 0);
			}
			double x = blueprint.getMin().get(i) + (broj / (Math.pow(2, brojBitova[i]) -1)) * blueprint.getVelicinaIntervala().get(i);
			p.set(i, x);
		}
		return p;
	}

	@Override
	public void ispisi() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < blueprint.getDuljina(); i++) {
			sb.append(bitovi.get(i) ? "1" : "0");
		}
		System.out.println(sb.toString());	
	}

	@Override
	public double ispitajDobrotu(IFunction fja) {
		return fja.getValueAtPoint(dekodiraj());
	}

	@Override
	public BinarnaJedinkaBlueprint getBlueprint() {
		return blueprint;
	}
	
	public boolean getBit(int bit) {
		return bitovi.get(bit);
	}
	
	public static BinarnaJedinka stvoriRandomJedinku(BinarnaJedinkaBlueprint blueprint) {
		BitSet bitovi = new BitSet(blueprint.getDuljina());
		Random rand = new Random();
		for(int i = 0; i < blueprint.getDuljina(); i++){
			bitovi.set(i, rand.nextBoolean());
		}
		return new BinarnaJedinka(bitovi, blueprint);
	}

}
