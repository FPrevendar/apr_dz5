package genetski;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import funkcije.IFunction;
import genetski.binarniKrizanja.IBinarniKrizanje;
import genetski.binarniMutacija.IBinarniMutacija;

public class BinarniGenetskiAlgoritam {
	
	class ParJedinkaDobrota{
		BinarnaJedinka jedinka;
		double dobrota;
	}
	
	private IBinarniKrizanje operacijaKrizanja;
	private IBinarniMutacija operacijaMutacije;
	private BinarnaJedinkaBlueprint blueprint;
	private List<ParJedinkaDobrota> populacija;
	private int velicinaPopulacije;
	private IFunction funkcijaCilja;
	private int brojIteracija;
	private boolean min;
	private ParJedinkaDobrota najboljaJedinka;
	
	public BinarniGenetskiAlgoritam(IBinarniMutacija mutacija, IBinarniKrizanje krizanje, BinarnaJedinkaBlueprint blueprint, int velicinaPopulacije, IFunction funckijaCilja, boolean min) {
		this.operacijaKrizanja = krizanje;
		this.operacijaMutacije = mutacija;
		this.blueprint = blueprint;
		this.velicinaPopulacije = velicinaPopulacije;
		this.populacija = new ArrayList<>();	
		this.funkcijaCilja = funckijaCilja;
		this.brojIteracija = 0;	
		this.min = min;
	}
	
	public BinarniGenetskiAlgoritam populiraj() {
		BinarnaJedinka jedinka = BinarnaJedinka.stvoriRandomJedinku(blueprint);
		najboljaJedinka = new ParJedinkaDobrota();
		najboljaJedinka.jedinka = jedinka;
		najboljaJedinka.dobrota = jedinka.ispitajDobrotu(funkcijaCilja);
		populacija.add(najboljaJedinka);
		for(int i = 1; i < velicinaPopulacije; i++) {
			jedinka = BinarnaJedinka.stvoriRandomJedinku(blueprint);
			ParJedinkaDobrota par = new ParJedinkaDobrota();
			double dobrota = jedinka.ispitajDobrotu(funkcijaCilja);
			par.jedinka = jedinka;
			par.dobrota = jedinka.ispitajDobrotu(funkcijaCilja);
			populacija.add(par);
			if(min && dobrota < najboljaJedinka.dobrota) {
				najboljaJedinka = par;
			}else if(!min && dobrota > najboljaJedinka.dobrota) {
				najboljaJedinka = par;
			}
		}
		return this;
	}
	
	public BinarniGenetskiAlgoritam provediIteraciju() {
		Random rand = new Random();
		int index1, index2, index3, najgoriIndex;
		index1 = (int)(rand.nextDouble() * velicinaPopulacije);
		while(index1 == (index2 = (int)(rand.nextDouble() * velicinaPopulacije)));
		while(index1 == (index3 = (int)(rand.nextDouble() * velicinaPopulacije)) || index2 == index3);
		ParJedinkaDobrota prva = populacija.get(index1);
		ParJedinkaDobrota druga = populacija.get(index2);
		ParJedinkaDobrota treca = populacija.get(index3);
		//System.out.println("Izabrao " + index1 + " " + index2 + " " + index3);
		if(min) {
			if(prva.dobrota > druga.dobrota) {
				najgoriIndex = index1;
			}else {
				najgoriIndex = index2;
			}
			if(treca.dobrota > populacija.get(najgoriIndex).dobrota) {
				najgoriIndex = index3;
			}
		}else {
			if(prva.dobrota < druga.dobrota) {
				najgoriIndex = index1;
			}else {
				najgoriIndex = index2;
			}
			if(treca.dobrota < populacija.get(najgoriIndex).dobrota) {
				najgoriIndex = index3;
			}
		}
		BinarnaJedinka novaJedinka;
		if(najgoriIndex == index1) {
			novaJedinka = operacijaKrizanja.krizaj(populacija.get(index2).jedinka, populacija.get(index3).jedinka);
		}else if(najgoriIndex == index2) {
			novaJedinka = operacijaKrizanja.krizaj(populacija.get(index1).jedinka, populacija.get(index3).jedinka);
		}else {
			novaJedinka = operacijaKrizanja.krizaj(populacija.get(index1).jedinka, populacija.get(index2).jedinka);
		}
		novaJedinka = operacijaMutacije.mutiraj(novaJedinka);
		ParJedinkaDobrota noviPar = new ParJedinkaDobrota();
		noviPar.jedinka = novaJedinka;
		noviPar.dobrota = novaJedinka.ispitajDobrotu(funkcijaCilja);
		populacija.set(najgoriIndex, noviPar);
		if(min && noviPar.dobrota < najboljaJedinka.dobrota) {
			najboljaJedinka = noviPar;
		}
		if(!min && noviPar.dobrota > najboljaJedinka.dobrota) {
			najboljaJedinka = noviPar;
		}
		brojIteracija++;
		return this;
	}
	
	public void ispisi() {
		System.out.println("Broj iteracija " + brojIteracija);
		System.out.println("Najbolja:");
		najboljaJedinka.jedinka.ispisi();
		najboljaJedinka.jedinka.dekodiraj().print();
		System.out.println("Dobrota:" + najboljaJedinka.dobrota);
		System.out.println();
		for(ParJedinkaDobrota par: populacija) {
			par.jedinka.ispisi();
			System.out.println("Dobrota " + par.dobrota);
		}
	}
	
	public BinarniGenetskiAlgoritam provediNIteracija(int n) {
		for(int i = 0; i < n; i++) {
			this.provediIteraciju();
		}
		return this;
	}
	
	public BinarnaJedinka getNajbolja() {
		return najboljaJedinka.jedinka;
	}
	
	public void ispisiNajbolji() {
		System.out.println("Broj iteracija " + brojIteracija);
		System.out.println("Najbolja:");
		najboljaJedinka.jedinka.ispisi();
		najboljaJedinka.jedinka.dekodiraj().print();
		System.out.println("Dobrota:" + najboljaJedinka.dobrota);
	}
}
