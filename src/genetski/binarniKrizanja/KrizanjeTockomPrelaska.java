package genetski.binarniKrizanja;

import java.util.BitSet;
import java.util.Random;

import genetski.BinarnaJedinka;

public class KrizanjeTockomPrelaska implements IBinarniKrizanje {
	
	Random rand = new Random();

	@Override
	public BinarnaJedinka krizaj(BinarnaJedinka b1, BinarnaJedinka b2) {
		int duljina = b1.getBlueprint().getDuljina();
		int indexPrelaska = (int) (rand.nextDouble() * duljina);
		BitSet bitovi = new BitSet(duljina);
		for(int i = 0; i < duljina; i++) {
			if(i < indexPrelaska) {
				bitovi.set(i, b1.getBit(i));
			}else {
				bitovi.set(i, b2.getBit(i));
			}
		}
		return new BinarnaJedinka(bitovi, b1.getBlueprint());
	}

}
