package genetski.binarniMutacija;

import java.util.BitSet;
import java.util.Random;

import genetski.BinarnaJedinka;

public class BinarnaMutacija implements IBinarniMutacija{
	
	private double vjerojatnostMutacije;
	private Random rand = new Random();
	
	public BinarnaMutacija(double p) {
		vjerojatnostMutacije = p;
	}

	@Override
	public BinarnaJedinka mutiraj(BinarnaJedinka b) {
		int duljina = b.getBlueprint().getDuljina();
		BitSet bitovi = new BitSet(duljina);
		for(int i = 0; i < duljina; i++) {
			if(rand.nextDouble() > vjerojatnostMutacije) {
				bitovi.set(i, b.getBit(i));
			}else {
				bitovi.set(i, !b.getBit(i));
			}
		}
		return new BinarnaJedinka(bitovi, b.getBlueprint());
	}

}
