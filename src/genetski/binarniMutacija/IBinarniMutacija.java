package genetski.binarniMutacija;

import genetski.BinarnaJedinka;

public interface IBinarniMutacija {
	
	public BinarnaJedinka mutiraj(BinarnaJedinka b);

}
