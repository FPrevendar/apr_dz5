package genetski.doubleKrizanja;

import java.util.Random;

import genetski.JedinkaSDouble;
import point.IPoint;
import point.Point;

public class KrizanjeIzborom implements IDoubleKrizanje {
	
	private Random rand = new Random();

	@Override
	public JedinkaSDouble krizaj(JedinkaSDouble prva, JedinkaSDouble druga) {
		IPoint p1 = prva.dekodiraj();
		IPoint p2 = druga.dekodiraj();
		
		IPoint rezPoint = new Point(p1.getDimenstion());
		for(int i = 0; i < rezPoint.getDimenstion(); i++) {
			if(rand.nextDouble() > 0.5) {
				rezPoint.set(i, p1.get(i));
			}else {
				rezPoint.set(i, p2.get(i));
			}
		}		
		return new JedinkaSDouble(rezPoint, prva.getBlueprint());
	}

}
