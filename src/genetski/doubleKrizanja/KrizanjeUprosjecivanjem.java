package genetski.doubleKrizanja;

import genetski.JedinkaSDouble;
import point.IPoint;

public class KrizanjeUprosjecivanjem implements IDoubleKrizanje{
	
	public JedinkaSDouble krizaj(JedinkaSDouble prva, JedinkaSDouble druga) {
		IPoint p = prva.dekodiraj();
		IPoint d = druga.dekodiraj();
		
		IPoint nova = p.add(d).mul(0.5);
		return new JedinkaSDouble(nova, prva.getBlueprint());
	}

}
