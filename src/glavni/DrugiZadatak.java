package glavni;

import java.io.FileWriter;
import java.io.IOException;

import matrix.IMatrix;
import matrix.Matrix;
import numerickiIntegrator.INumerickiIntegrator;
import numerickiIntegrator.NumerickiIntegratorUtil;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegrator;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegratorKorektor;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegratorPrediktor;

public class DrugiZadatak {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Drugi Zadatak");
		
		IMatrix A = Matrix.parseMatrix("0 1\n -200 -102");
		IMatrix x0 = Matrix.parseMatrix("1\n-2");
		
		double T = 0.1;
		double tmax = 1;
		
		FileWriter file = new FileWriter("DrugiZadatak.txt");
		file.write("T " + T + "\n");
		file.write("tmax " + tmax + "\n");
		file.write("dim = 2\n");
		file.write("X0 = " + x0.transposed().toString() + "\n");
		
		INumerickiIntegrator euler = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.EULER, A, T, x0);
		INumerickiIntegrator obrnutiEuler = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.OBRNUTI_EULER, A, T, x0);
		INumerickiIntegrator trapezni = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.TRAPEZNI, A, T, x0);
		INumerickiIntegrator rungeKutta = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.RUNGEKUTTA4REDA, A, T, x0);
		INumerickiIntegrator pece2Euler = NumerickiIntegratorUtil.stvoriPece(EnumIntegratorPrediktor.EULER,
				EnumIntegratorKorektor.OBRNUTI_EULER,
				A, T, x0, 2);
		INumerickiIntegrator peceEulerTrapez = NumerickiIntegratorUtil.stvoriPece(EnumIntegratorPrediktor.EULER,
				EnumIntegratorKorektor.TRAPEZNI,
				A, T, x0, 1);
		
		IMatrix[] eulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(euler, T, tmax);
		IMatrix[] obrnutiEulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(obrnutiEuler, T, tmax);
		IMatrix[] trapezniVrijednosti = NumerickiIntegratorUtil.provediIteracije(trapezni, T, tmax);
		IMatrix[] rungeKuttaVrijednosti = NumerickiIntegratorUtil.provediIteracije(rungeKutta, T, tmax);
		IMatrix[] pece2EulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(pece2Euler, T, tmax);
		IMatrix[] peceEulerTrapezVrijednosti = NumerickiIntegratorUtil.provediIteracije(peceEulerTrapez, T, tmax);
		
		file.write("Euler\n");
		for(IMatrix m: eulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Obrnuti Euler\n");
		for(IMatrix m: obrnutiEulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Trapezni\n");
		for(IMatrix m: trapezniVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Runge-Kutta 4. reda\n");
		for(IMatrix m: rungeKuttaVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("PE(CE)2 Euler\n");
		for(IMatrix m: pece2EulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("PECE Euler Trapezni\n");
		for(IMatrix m: peceEulerTrapezVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		file.close();
		System.out.println("Rezultati zapisani u DrugiZadatak.txt");
	}

}
