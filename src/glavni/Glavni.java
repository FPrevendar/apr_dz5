package glavni;

import java.io.IOException;

public class Glavni {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Peti labos");
		PrviZadatak.main(args);
		DrugiZadatak.main(args);
		TreciZadatak.main(args);
		CetvrtiZadatak.main(args);
	}

}
