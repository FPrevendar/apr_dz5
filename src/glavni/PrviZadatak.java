package glavni;

import java.io.FileWriter;
import java.io.IOException;

import matrix.IMatrix;
import matrix.Matrix;
import numerickiIntegrator.INumerickiIntegrator;
import numerickiIntegrator.NumerickiIntegratorUtil;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegrator;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegratorKorektor;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegratorPrediktor;

public class PrviZadatak {
	public static void main(String[] args) throws IOException {
		System.out.println("Prvi Zadatak");
		
		IMatrix A = Matrix.parseMatrix("0 1\n -1 0");
		IMatrix x0 = Matrix.parseMatrix("1\n 1");
		
		double T = 0.01;
		double tmax = 10;
		
		
		FileWriter file = new FileWriter("PrviZadatak.txt");
		file.write("T " + T + "\n");
		file.write("tmax " + tmax + "\n");
		file.write("dim = 2\n");
		file.write("X0 = " + x0.transposed().toString() + "\n");
		
		INumerickiIntegrator euler = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.EULER, A, T, x0);
		INumerickiIntegrator obrnutiEuler = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.OBRNUTI_EULER, A, T, x0);
		INumerickiIntegrator trapezni = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.TRAPEZNI, A, T, x0);
		INumerickiIntegrator rungeKutta = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.RUNGEKUTTA4REDA, A, T, x0);
		INumerickiIntegrator pece2Euler = NumerickiIntegratorUtil.stvoriPece(EnumIntegratorPrediktor.EULER,
				EnumIntegratorKorektor.OBRNUTI_EULER,
				A, T, x0, 2);
		INumerickiIntegrator peceEulerTrapez = NumerickiIntegratorUtil.stvoriPece(EnumIntegratorPrediktor.EULER,
				EnumIntegratorKorektor.TRAPEZNI,
				A, T, x0, 1);
		
		IMatrix[] eulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(euler, T, tmax);
		IMatrix[] obrnutiEulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(obrnutiEuler, T, tmax);
		IMatrix[] trapezniVrijednosti = NumerickiIntegratorUtil.provediIteracije(trapezni, T, tmax);
		IMatrix[] rungeKuttaVrijednosti = NumerickiIntegratorUtil.provediIteracije(rungeKutta, T, tmax);
		IMatrix[] pece2EulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(pece2Euler, T, tmax);
		IMatrix[] peceEulerTrapezVrijednosti = NumerickiIntegratorUtil.provediIteracije(peceEulerTrapez, T, tmax);
		
		IMatrix[] egzaktno = new IMatrix[(int)Math.ceil(tmax/T)]; 
		double tt = 0;
		for(int iteracija = 0; iteracija < (int)Math.ceil(tmax/T); iteracija++) {
			IMatrix vrijednost = new Matrix(2,1);
			tt+= T;
			vrijednost.set(0, 0, x0.get(0, 0) * Math.cos(tt) + x0.get(1, 0) * Math.sin(tt));
			vrijednost.set(1, 0, x0.get(1, 0) * Math.cos(tt) - x0.get(0, 0) * Math.sin(tt));
			egzaktno[iteracija] = vrijednost;
		}
		
		file.write("Egzaktno\n");
		for(IMatrix m: egzaktno) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Euler\n");
		for(IMatrix m: eulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Obrnuti Euler\n");
		for(IMatrix m: obrnutiEulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Trapezni\n");
		for(IMatrix m: trapezniVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Runge-Kutta 4. reda\n");
		for(IMatrix m: rungeKuttaVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("PE(CE)2 Euler\n");
		for(IMatrix m: pece2EulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("PECE Euler Trapezni\n");
		for(IMatrix m: peceEulerTrapezVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		file.close();
		System.out.println("Rezultati zapisani u PrviZadatak.txt");
		
		
		double[] eulerGreske = new double[2];
		double[] obrnutiEulerGreske = new double[2];
		double[] trapezniGreske = new double[2];
		double[] rungeKuttaGreske = new double[2];
		double[] pece2EulerGreske = new double[2];
		double[] peceEulerTrapezGreske = new double[2];
		
		for(int i = 0; i < egzaktno.length; i++) {
			eulerGreske[0]+= Math.abs(eulerVrijednosti[i].get(0, 0) - egzaktno[i].get(0, 0));
			eulerGreske[1]+= Math.abs(eulerVrijednosti[i].get(1, 0) - egzaktno[i].get(1, 0));
			
			obrnutiEulerGreske[0]+= Math.abs(obrnutiEulerVrijednosti[i].get(0, 0) - egzaktno[i].get(0, 0));
			obrnutiEulerGreske[1]+= Math.abs(obrnutiEulerVrijednosti[i].get(1, 0) - egzaktno[i].get(1, 0));
			
			trapezniGreske[0]+= Math.abs(trapezniVrijednosti[i].get(0, 0) - egzaktno[i].get(0, 0));
			trapezniGreske[1]+= Math.abs(trapezniVrijednosti[i].get(1, 0) - egzaktno[i].get(1, 0));
			
			rungeKuttaGreske[0]+= Math.abs(rungeKuttaVrijednosti[i].get(0, 0) - egzaktno[i].get(0, 0));
			rungeKuttaGreske[1]+= Math.abs(rungeKuttaVrijednosti[i].get(1, 0) - egzaktno[i].get(1, 0));
			
			pece2EulerGreske[0]+= Math.abs(pece2EulerVrijednosti[i].get(0, 0) - egzaktno[i].get(0, 0));
			pece2EulerGreske[1]+= Math.abs(pece2EulerVrijednosti[i].get(1, 0) - egzaktno[i].get(1, 0));
			
			peceEulerTrapezGreske[0]+= Math.abs(peceEulerTrapezVrijednosti[i].get(0, 0) - egzaktno[i].get(0, 0));
			peceEulerTrapezGreske[1]+= Math.abs(peceEulerTrapezVrijednosti[i].get(1, 0) - egzaktno[i].get(1, 0));
		}
		System.out.println("Euler greske = " + eulerGreske[0] + " " + eulerGreske[1]);
		System.out.println("Obrnuti Euler greske = " + obrnutiEulerGreske[0] + " " + obrnutiEulerGreske[1]);
		System.out.println("Trapezni greske = " + trapezniGreske[0] + " " + trapezniGreske[1]);
		System.out.println("Runge-Kutta greske = " + rungeKuttaGreske[0] + " " + rungeKuttaGreske[1]);
		System.out.println("PE(CE)2 Euler greske = " + pece2EulerGreske[0] + " " + pece2EulerGreske[1]);
		System.out.println("PECE Euler trapez greske = " + peceEulerTrapezGreske[0] + " " + peceEulerTrapezGreske[1]);
	}

}
