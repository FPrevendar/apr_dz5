package glavni;

import java.io.FileWriter;
import java.io.IOException;

import matrix.IMatrix;
import matrix.Matrix;
import numerickiIntegrator.INumerickiIntegrator;
import numerickiIntegrator.NumerickiIntegratorUtil;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegrator;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegratorKorektor;
import numerickiIntegrator.NumerickiIntegratorUtil.EnumIntegratorPrediktor;
import numerickiIntegrator.RtFunkcija;
import numerickiIntegrator.RtFunkcijaJedinice;

public class TreciZadatak {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Treci Zadatak");
		IMatrix A = Matrix.parseMatrix("0 -2\n1 -3");
		IMatrix B = Matrix.parseMatrix("2 0\n0 3");
		RtFunkcija rt = new RtFunkcijaJedinice(2);
		IMatrix x0 = Matrix.parseMatrix("1\n3");
		
		double T = 0.01;
		double tmax = 10;
		
		FileWriter file = new FileWriter("TreciZadatak.txt");
		file.write("T " + T + "\n");
		file.write("tmax " + tmax + "\n");
		file.write("dim = 2\n");
		file.write("X0 = " + x0.transposed().toString() + "\n");
		
		INumerickiIntegrator euler = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.EULER, A, B, T, rt, x0);
		INumerickiIntegrator obrnutiEuler = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.OBRNUTI_EULER, A, B, T, rt, x0);
		INumerickiIntegrator trapezni = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.TRAPEZNI, A, B, T, rt, x0);
		INumerickiIntegrator rungeKutta = NumerickiIntegratorUtil.stvoriIntegrator(EnumIntegrator.RUNGEKUTTA4REDA, A, B, T, rt, x0);
		INumerickiIntegrator pece2Euler = NumerickiIntegratorUtil.stvoriPece(EnumIntegratorPrediktor.EULER,
				EnumIntegratorKorektor.OBRNUTI_EULER,
				A, B, T, rt, x0, 2);
		INumerickiIntegrator peceEulerTrapez = NumerickiIntegratorUtil.stvoriPece(EnumIntegratorPrediktor.EULER,
				EnumIntegratorKorektor.TRAPEZNI,
				A, B, T, rt, x0, 1);
		
		IMatrix[] eulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(euler, T, tmax);
		IMatrix[] obrnutiEulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(obrnutiEuler, T, tmax);
		IMatrix[] trapezniVrijednosti = NumerickiIntegratorUtil.provediIteracije(trapezni, T, tmax);
		IMatrix[] rungeKuttaVrijednosti = NumerickiIntegratorUtil.provediIteracije(rungeKutta, T, tmax);
		IMatrix[] pece2EulerVrijednosti = NumerickiIntegratorUtil.provediIteracije(pece2Euler, T, tmax);
		IMatrix[] peceEulerTrapezVrijednosti = NumerickiIntegratorUtil.provediIteracije(peceEulerTrapez, T, tmax);
		
		file.write("Euler\n");
		for(IMatrix m: eulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Obrnuti Euler\n");
		for(IMatrix m: obrnutiEulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Trapezni\n");
		for(IMatrix m: trapezniVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("Runge-Kutta 4. reda\n");
		for(IMatrix m: rungeKuttaVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("PE(CE)2 Euler\n");
		for(IMatrix m: pece2EulerVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		
		file.write("PECE Euler Trapezni\n");
		for(IMatrix m: peceEulerTrapezVrijednosti) {
			file.write(m.transposed().toString() + "\n");
		}
		file.close();
		System.out.println("Rezultati zapisani u TreciZadatak.txt");
	}

}
