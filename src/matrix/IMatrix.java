package matrix;

/**
 * Sucelje koje predstavlja matricu
 */
public interface IMatrix {
	
	// broj redaka/stupaca
	public int sirina();
	public int visina();
	
	// engleske verzije
	public int width();
	public int height();
	
	// metode za dohvacanje i postavljanje elemenata
	// set modificira matricu
	public double get(int i, int j);
	public void set(int i, int j, double z);
	
	// metode za osvjezavanje pojedinih elemenata
	// modificiraju matricu
	public IMatrix addElem(int i, int j, double toAdd);
	public IMatrix subElem(int i, int j, double toSub);
	public IMatrix mulElem(int i, int j, double toMul);
	public IMatrix divElem(int i, int j, double toDiv);
	
	// metode za osvjezavanje matrice skalarima
	// modificiraju matricu
	// slicno kao i gore, sam za svaki element
	public IMatrix add(double toAdd);
	public IMatrix sub(double toSub);
	public IMatrix mul(double toMul);
	public IMatrix div(double toDiv);
	
	// metode za operacije s dve matrice
	// matrice ostaju nepromijenjene
	public IMatrix add(IMatrix toAdd);
	public IMatrix sub(IMatrix toSub);
	public IMatrix mul(IMatrix toMul);
	
	// metoda za kopiranje
	public IMatrix copy();
	
	// transponiranje
	// vraca transponiranu kopiju
	public IMatrix transposed();
	
	// ispis matrice
	public void print();
	
	// ispis matrice u file
	void writeMatrixToFile(String filename);
	
	// usporedba dvije matrice
	public boolean compare(IMatrix other);
	
	// supstitucija unaprijed i unatrag
	IMatrix supstitucijaUnatrag(IMatrix matrica);
	IMatrix supstitucijaUnaprijed(IMatrix matrica);
	
	// LU dekompozicija
	// modificira matricu i vraca ju
	public IMatrix LUdekompozicija();
	
	// zamjena redaka i stupaca
	// modificira matricu
	public IMatrix swapRows(int r1, int r2);
	public IMatrix swapCols(int c1, int c2);
	
	// LUP dekompoticija
	// modificira matricu, (PAZI!!!) vraca
	// vraca primjerak posebne static klase
	static class LUPRjesenje{
		public int detP;
		public IMatrix P;
		public IMatrix LU;
	}
	public LUPRjesenje LUPdekompozicija();
	
	// inverz
	public IMatrix inverz();
	
	// determinanta
	public double determinanta();
	
	public String toString();
	
}
