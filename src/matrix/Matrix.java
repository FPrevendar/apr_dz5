package matrix;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author filip
 */
public class Matrix implements IMatrix {
	
	private int sirina;
	private int visina;
	
	private double[][] polje;
	
	// eps je mjera je li neki broj blitak nuli
	private static double eps = 10e-6;
	// tol je mjera jesu li dva broja ista
	private static double tol = 10e-6;
	
	// konstruktor, stvara praznu matricu danih dimenzija
	public Matrix(int visina, int sirina) {
		this.sirina = sirina;
		this.visina = visina;
		polje = new double[visina][sirina];
	};
	
	// geteri i seteri za tol i eps
	public static double getEps() {
		return eps;
	}
	
	public static void setEps(double newEps) {
		eps = newEps;
	}
	
	public static double getTol() {
		return tol;
	}
	
	public static void setTol(double newTol) {
		tol = newTol;
	}
	
	// privatna metoda za provjeru granica
	private boolean checkBounds(int n, int m) {
		if(n < 0 || m < 0 || m >= sirina || n >= visina) {
			return false;
		}else {
			return true;
		}
	}
	
	// staticne metode tvornice
	
	// daje matricu dimenzija nxn, gdje su jedinice na dijagonali
	public static IMatrix identity(int n) {
		IMatrix m = new Matrix(n,n);
		for(int i = 0; i < n; i++) {
			m.set(i, i, 1);
		}
		return m;
	}
	
	// ucitava matricu iz stringa
	public static IMatrix parseMatrix(String s) {
		try {
		String[] redovi = s.trim().split("\n");
		String[][] brojevi = new String[redovi.length][];
		for(int i = 0; i < redovi.length; i++) {
			brojevi[i] = redovi[i].trim().split("\\s+");
		}
		IMatrix mat = new Matrix(redovi.length, brojevi[0].length);
		for(int i = 0; i < redovi.length; i++) {
			for(int j = 0; j < brojevi[0].length; j++) {
				mat.set(i, j, Double.parseDouble(brojevi[i][j]));
			}
		}
		return mat;
		}catch(Exception e) {
			throw new IllegalArgumentException("Greska pri parsiranju matrice");
		}
	}
	
	// ucitava matricu iz fajla
	public static IMatrix parseMatrixFromFile(String filename){
		try {
			List<String> redovi = Files.readAllLines(Paths.get(filename));
			String[][] brojevi = new String[redovi.size()][];
			for(int i = 0; i < redovi.size(); i++) {
				brojevi[i] = redovi.get(i).trim().split("\\s+");
			}
			IMatrix mat = new Matrix(redovi.size(), brojevi[0].length);
			for(int i = 0; i < redovi.size(); i++) {
				for(int j = 0; j < brojevi[0].length; j++) {
					mat.set(i, j, Double.parseDouble(brojevi[i][j]));
				}
			}
			return mat;
		}catch(Exception e) {
			throw new RuntimeException("greska pri parsiranju matrice iz datoteke");
		}
	}

	@Override
	public int sirina() {
		return sirina;
	}

	@Override
	public int visina() {
		return visina;
	}

	@Override
	public int width() {
		return sirina;
	}

	@Override
	public int height() {
		return visina;
	}

	// dohvacanje elementa
	@Override
	public double get(int i, int j) {
		if(checkBounds(i, j)) {
			return polje[i][j];
		}else {
			throw new ArrayIndexOutOfBoundsException("pristupa se " + i + ", " + j);
		}
	}

	// postavlanje elementa
	@Override
	public void set(int i, int j, double z) {
		if(checkBounds(i, j)) {
			polje[i][j] = z;
		}else {
			throw new ArrayIndexOutOfBoundsException("pristupa se" + i + ", " + j);
		}
	}

	// operacije za osvjezavanje jednog elementa (ekvivalentni +=, -=, *=, /= operatorima)
	@Override
	public IMatrix addElem(int i, int j, double toAdd) {
		if(checkBounds(i, j)) {
			polje[i][j] += toAdd;
			return this;
		}else {
			throw new ArrayIndexOutOfBoundsException("pristupa se" + i + ", " + j);
		}
	}

	@Override
	public IMatrix subElem(int i, int j, double toSub) {
		if(checkBounds(i, j)) {
			polje[i][j] -= toSub;
			return this;
		}else {
			throw new ArrayIndexOutOfBoundsException("pristupa se" + i + ", " + j);
		}
	}

	@Override
	public IMatrix mulElem(int i, int j, double toMul) {
		if(checkBounds(i, j)) {
			polje[i][j] *= toMul;
			return this;
		}else {
			throw new ArrayIndexOutOfBoundsException("pristupa se" + i + ", " + j);
		}
	}

	@Override
	public IMatrix divElem(int i, int j, double toDiv) {
		if(Math.abs(toDiv) < eps) {
			throw new IllegalArgumentException("Djelitelj je broj blizak nuli");
		}
		if(checkBounds(i, j)) {
			polje[i][j] /= toDiv;
			return this;
		}else {
			throw new ArrayIndexOutOfBoundsException("pristupa se" + i + ", " + j);
		}
	}

	// metode za osvjezavanje svih elemenata matrice
	@Override
	public IMatrix add(double toAdd) {
		for(int i = 0; i < visina; i++) {
			for(int j = 0; j < sirina; j++) {
				polje[i][j] += toAdd;
			}
		}
		return this;
	}

	@Override
	public IMatrix sub(double toSub) {
		for(int i = 0; i < visina; i++) {
			for(int j = 0; j < sirina; j++) {
				polje[i][j] -= toSub;
			}
		}
		return this;
	}

	@Override
	public IMatrix mul(double toMul) {
		for(int i = 0; i < visina; i++) {
			for(int j = 0; j < sirina; j++) {
				polje[i][j] *= toMul;
			}
		}
		return this;
	}

	@Override
	public IMatrix div(double toDiv) {
		if(Math.abs(toDiv) < eps) {
			throw new IllegalArgumentException("Djelitelj je broj blizak nuli");
		}
		for(int i = 0; i < visina; i++) {
			for(int j = 0; j < sirina; j++) {
				polje[i][j] /= toDiv;
			}
		}
		return this;
	}

	// operacije s dvije matrice
	@Override
	public IMatrix add(IMatrix toAdd) {
		if(this.sirina != toAdd.sirina() || this.visina != toAdd.visina()) {
			throw new IllegalArgumentException("Matrice nisu istih dimenzija");
		}
		IMatrix nova = new Matrix(visina, sirina);
		for(int i = 0; i < visina; i++) {
			for(int j = 0; j < sirina; j++) {
				nova.set(i, j, this.get(i, j) + toAdd.get(i, j));
			}
		}
		return nova;
	}

	@Override
	public IMatrix sub(IMatrix toSub) {
		if(this.sirina != toSub.sirina() || this.visina != toSub.visina()) {
			throw new IllegalArgumentException("Matrice nisu istih dimenzija");
		}
		IMatrix nova = new Matrix(visina, sirina);
		for(int i = 0; i < visina; i++) {
			for(int j = 0; j < sirina; j++) {
				nova.set(i, j, this.get(i, j) - toSub.get(i, j));
			}
		}
		return nova;
	}

	@Override
	public IMatrix mul(IMatrix toMul) {
		if(this.sirina() != toMul.visina()) {
			throw new IllegalArgumentException("Matrice nisu kompatibilne");
		}
		int n = this.visina;
		int m = this.sirina;
		int p = toMul.sirina();
		IMatrix nova = new Matrix(n,p);
		
		for(int i = 0; i < n; i++) {
			for(int k = 0; k < p; k++) {
				nova.set(i, k, 0);
				for(int j = 0; j < m; j++) {
					nova.addElem(i, k, this.get(i, j) * toMul.get(j, k));
				}
			}
		}
		
		return nova;
	}

	// vraca kopiju matrice
	@Override
	public IMatrix copy() {
		IMatrix nova = new Matrix(visina, sirina);
		
		for(int x = 0; x < sirina; x++) {
			for(int y = 0; y < visina; y++) {
				nova.set(y, x, this.get(y, x));
			}
		}
		return nova;
	}

	// vraca transponiranu kopiju matrice
	@Override
	public IMatrix transposed() {
		IMatrix nova = new Matrix(sirina, visina);
		
		for(int x = 0; x < sirina; x++) {
			for(int y = 0; y < visina; y++) {
				nova.set(x, y, this.get(y, x));
			}
		}
		return nova;
	}

	// ispisuje matricu na ekran
	@Override
	public void print() {
		for(double[] red : polje) {
			for(double elem: red) {
				System.out.print(elem + " ");
			}
			System.out.print("\n");
		}
	}

	// usporedjuje dvije matrice, koristi tol
	@Override
	public boolean compare(IMatrix other) {
		if(this.sirina != other.sirina() || this.visina != other.visina()) {
			return false;
		}
		for(int i = 0; i < visina; i++) {
			for (int j = 0; j < sirina; j++) {
				if(Math.abs(this.get(i, j) - other.get(i, j)) > tol) {
					return false;
				}
			}
		}
		return true;
	}
	
	// sprema matricu u fajl u ljudski citljivom obliku
	@Override
	public void writeMatrixToFile(String filename) {
		try {
			FileWriter file = new FileWriter(filename);
			for(double[] red : polje) {
				for(double elem: red) {
					file.write(Double.toString(elem));
					file.write(" ");
				}
				file.write("\n");
			}
			file.close();
		}catch(Exception e){
			throw new RuntimeException("Greska pri ispisu");
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(double[] red : polje) {
			for(double elem: red) {
				sb.append(Double.toString(elem));
				sb.append(" ");
			}
			sb.append("\n");
		}
		sb.deleteCharAt(sb.length() - 1); // obrise zadnji \n
		return sb.toString();
	}
	
	
	// supstitucija unaprijed, pozivajuci objekt mora biti vektor (sirina mu je 1),
	// a argument mora biti kvadratna matrica
	// iste visine kao i vektor
	@Override
	public IMatrix supstitucijaUnaprijed(IMatrix matrica) {
		if(this.sirina() != 1) {
			throw new IllegalArgumentException("Supstitucija Unaprijed se radi s vektorom");
		}else if(this.visina() != matrica.sirina() && this.visina() != matrica.visina()) {
			throw new IllegalArgumentException("Matica nije odgovarajuce velicine");
		}	
		for (int i = 0; i < this.visina() - 1; i++) {
			for(int j = i + 1; j < this.visina(); j++) {
				this.subElem(j, 0, matrica.get(j, i) * this.get(i, 0));
			}
		}
		return this;
	}
	
	// supstitucija unatrag, vrijede ista pravila kao i za supstituciju unatrag
	@Override
	public IMatrix supstitucijaUnatrag(IMatrix matrica) {
		if(this.sirina() != 1) {
			throw new IllegalArgumentException("Supstitucija Unaprijed se radi s vektorom");
		}else if(this.visina() != matrica.sirina() && this.visina() != matrica.visina()) {
			throw new IllegalArgumentException("Matica nije odgovarajuce velicine");
		}
		for(int i = this.visina() - 1; i >= 0; i--) {
			this.divElem(i, 0, matrica.get(i, i));
			for(int j = 0; j < i; j++) {
				this.subElem(j, 0, matrica.get(j, i) * this.get(i, 0));
			}
		}
		return this;
	}

	// metoda za racunanje LU dekompozicije kvadratne matrice
	// modificira matricu i vraca ju
	@Override
	public IMatrix LUdekompozicija() {
		if(this.visina() != this.sirina()) {
			throw new IllegalArgumentException("Matrica nije kvadratna");
		}
		try {
			for(int i = 0; i < this.visina() - 1; i++) {
				for(int j = i + 1; j < this.visina(); j++) {
					this.divElem(j, i, this.get(i, i));
					for(int k = i + 1; k < this.visina(); k++) {
						this.subElem(j, k, this.get(j, i) * this.get(i, k));
					}
				}
			}
			return this;
		}catch(Exception e) {
			throw new IllegalArgumentException("Matrica se ne moze dekomponirati LU dekompozicijom");
		}
	}

	// zamjene redaka i stupaca
	@Override
	public IMatrix swapRows(int r1, int r2) {
		if(r1 < 0 || r2 < 0 || r1 >= this.visina() || r2 >= this.visina()) {
			throw new IllegalArgumentException("Pokusavaju se zamijeniti krivi redovi");
		}
		double[] pomRed = this.polje[r1];
		this.polje[r1] = this.polje[r2];
		this.polje[r2] = pomRed;
		return this;
	}

	@Override
	public IMatrix swapCols(int c1, int c2) {
		if(c1 < 0 || c2 < 0 || c1 >= this.sirina() || c2 >= this.sirina()) {
			throw new IllegalArgumentException("Pokusavaju se zamijeniti krivi stupci");
		}
		for(int i = 0; i < this.visina(); i++) {
			double broj = this.polje[i][c1];
			this.polje[i][c1] = this.polje[i][c2];
			this.polje[i][c2] = broj;
		}
		return this;
	}

	// metoda za obavljanje LUP dekompozicije
	// matrica mora biti kvadratna
	// modificira matricu
	// povratna vrijednost je primjerak staticne klase LUPRjesenje
	@Override
	public LUPRjesenje LUPdekompozicija() {
		
		if(this.visina() != this.sirina()) {
			throw new IllegalArgumentException("Matrica nije kvadratna");
		}
		
		IMatrix p = Matrix.identity(this.sirina());
		LUPRjesenje rjesenje = new LUPRjesenje();
		int promjena = 1; // ovo je bitno samo za racunanje determinante
		
		for(int i = 0; i < this.visina() - 1; i++) {
			int pivot = this.odaberiPivot(i);
			if(Math.abs(this.get(pivot,i)) < Matrix.eps) {
				throw new IllegalArgumentException("Najbolji pivot je blizak nuli");
			}
			if(i != pivot) {
				this.swapRows(i, pivot);
				p.swapRows(i, pivot);
				promjena *= -1;
			}
			for(int j = i + 1; j < this.sirina(); j++) {
				this.divElem(j, i, this.get(i, i));
				for(int k = i + 1; k < this.sirina(); k++) {
					this.subElem(j, k, this.get(j, i) *  this.get(i, k));
				}
			}
		}
		
		rjesenje.detP = promjena;
		rjesenje.LU = this;
		rjesenje.P = p;
		return rjesenje;
	}
	
	// privatna metoda za pronalazenje najboljeg pivota
	private int odaberiPivot(int red) {
		double max = Math.abs(this.get(red, red));
		int maxRed = red;
		for(int i = red; i < this.visina(); i++) {
			if(Math.abs(this.get(i, red)) > max) {
				max = Math.abs(this.get(i, red));
				maxRed = i;
			}
		}
		return maxRed;
	}

	// staticne metode za rjesavanje sustava, ne diraju matrice vec rade kopije matrica
	// metode samo omotavaju metode za supstituciju i dekompoziciju
	// a je matrica, a b je vektor
	public static IMatrix rjesiSustavLUdekompozicijom(IMatrix a, IMatrix b) {
		IMatrix A = a.copy();
		IMatrix B = b.copy();
		
		A.LUdekompozicija();
		B = B.supstitucijaUnaprijed(A).supstitucijaUnatrag(A);
		return B;
	}

	public static IMatrix rjesiSustavLUPdekompozicijom(IMatrix a, IMatrix b) {
		IMatrix A = a.copy();
		IMatrix B = b.copy();
		LUPRjesenje lupRj = A.LUPdekompozicija();
		IMatrix P = lupRj.P;
		B = P.mul(B);
		B = B.supstitucijaUnaprijed(A).supstitucijaUnatrag(A);
		return B;
	}

	// racunanje inverza matrice preko LUP dekompozicije
	@Override
	public IMatrix inverz() {
		IMatrix A = this.copy();
		LUPRjesenje lupRj = A.LUPdekompozicija();
		IMatrix P = lupRj.P;
		IMatrix inverz = new Matrix(A.visina(), A.sirina());
		int n = A.visina();
		for(int i = 0; i < n; i++) {
			IMatrix ei = new Matrix(n, 1);
			ei.addElem(i, 0, 1);
			ei = P.mul(ei);
			ei.supstitucijaUnaprijed(A).supstitucijaUnatrag(A);
			for(int j = 0; j < n; j++) {
				inverz.set(j, i, ei.get(j, 0));
			}
		}
		return inverz;
	}
	
	// recunanje determinante preko LUP dekompozicije
	@Override
	public double determinanta() {
		IMatrix a = this.copy();
		LUPRjesenje lupRj = a.LUPdekompozicija();
		double det = lupRj.detP;
		for(int i = 0; i < a.visina(); i++) {
			det *= a.get(i, i);
		}
		return det;
	}
}
