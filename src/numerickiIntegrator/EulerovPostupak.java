package numerickiIntegrator;

import matrix.IMatrix;

public class EulerovPostupak implements INumerickiIntegrator, IntegratorPrediktor{
	
	private IMatrix A;
	private IMatrix B;
	private IMatrix xk;
	private double T;
	private RtFunkcija rt;
	private double t;
	
	public EulerovPostupak(double T, IMatrix A, IMatrix B, IMatrix xk, RtFunkcija rt) {
		this.A = A;
		this.B = B;
		this.T = T;
		this.rt = rt;
		this.xk = xk;
		this.t = 0;
	}
	
	@Override
	public IMatrix provediIteraciju() {
		IMatrix m = xk.add(A.mul(xk).add(B.mul(rt.evaluate(t))).copy().mul(T));
		return m;
	}
	
	@Override
	public void postavi(IMatrix xk, double t) {
		this.xk = xk;
		this.t = t;
	}

	@Override
	public IMatrix predvidi() {
		return provediIteraciju();
	}
}
