package numerickiIntegrator;

import matrix.IMatrix;

public interface INumerickiIntegrator {

	public IMatrix provediIteraciju();
	
	public void postavi(IMatrix xk, double t);

}
