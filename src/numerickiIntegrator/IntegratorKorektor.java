package numerickiIntegrator;

import matrix.IMatrix;

public interface IntegratorKorektor extends INumerickiIntegrator{
	
	public IMatrix korigiraj(IMatrix predikcija);

}
