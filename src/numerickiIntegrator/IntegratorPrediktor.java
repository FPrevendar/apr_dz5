package numerickiIntegrator;

import matrix.IMatrix;

public interface IntegratorPrediktor extends INumerickiIntegrator{
	
	public IMatrix predvidi();

}
