package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class NumerickiIntegratorUtil {
	
	public enum EnumIntegrator{
		EULER,
		OBRNUTI_EULER,
		TRAPEZNI,
		RUNGEKUTTA4REDA
	}
	
	public enum EnumIntegratorPrediktor{
		EULER,
		RUNGEKUTTA4REDA
	}
	
	public enum EnumIntegratorKorektor{
		OBRNUTI_EULER,
		TRAPEZNI
	}
	
	public static INumerickiIntegrator stvoriIntegrator(EnumIntegrator integrator,
			IMatrix A,
			IMatrix B,
			double T,
			RtFunkcija rt,
			IMatrix xt0) {
		if(integrator == EnumIntegrator.EULER) {
			return new EulerovPostupak(T, A, B, xt0, rt);
		}else if(integrator == EnumIntegrator.OBRNUTI_EULER) {
			return new ObrnutiEulerovPostupak(T, A, B, xt0, rt);
		}else if(integrator == EnumIntegrator.RUNGEKUTTA4REDA) {
			return new RungeKutta4RedaPostupak(T, A, B, xt0, rt);
		}else if(integrator == EnumIntegrator.TRAPEZNI) {
			return new TrapezniPostupak(T, A, B, xt0, rt);
		}else {
			throw new IllegalArgumentException("Nesto je krivo pri stvaranju integratora");
		}
	}
	
	public static INumerickiIntegrator stvoriIntegrator(EnumIntegrator integrator,
			IMatrix A,
			double T,
			IMatrix xt0) {
		return stvoriIntegrator(integrator,
				A,
				new Matrix(A.visina(), A.visina()),
				T,
				new RtFunkcijaNule(A.visina()),
				xt0);
	}
	
	public static INumerickiIntegrator stvoriPece(EnumIntegratorPrediktor pred,
			EnumIntegratorKorektor kore,
			IMatrix A,
			IMatrix B,
			double T,
			RtFunkcija rt,
			IMatrix xt0,
			int brojKorekcija) {
		IntegratorPrediktor prediktor = null;
		if(pred == EnumIntegratorPrediktor.EULER) {
			prediktor = new EulerovPostupak(T, A, B, xt0, rt);
		}else if(pred == EnumIntegratorPrediktor.RUNGEKUTTA4REDA) {
			prediktor = new RungeKutta4RedaPostupak(T, A, B, xt0, rt);
		}
		IntegratorKorektor korektor = null;
		if(kore == EnumIntegratorKorektor.OBRNUTI_EULER) {
			korektor = new ObrnutiEulerovPostupak(T, A, B, xt0, rt);
		}else if(kore == EnumIntegratorKorektor.TRAPEZNI) {
			korektor = new TrapezniPostupak(T, A, B, xt0, rt);
		}
		return new PECEPostupak(prediktor, korektor, brojKorekcija, T, A, B, xt0, rt);
	}
	
	public static INumerickiIntegrator stvoriPece(EnumIntegratorPrediktor pred,
			EnumIntegratorKorektor korektor,
			IMatrix A,
			double T,
			IMatrix xt0,
			int brojKorekcija) {
		return stvoriPece(pred,
				korektor,
				A,
				new Matrix(A.visina(),A.visina()),
				T,
				new RtFunkcijaNule(A.visina()),
				xt0,
				brojKorekcija);
	}
	
	public static IMatrix[] provediIteracije(INumerickiIntegrator integrator, double T, double tmax) {
		int brojIteracija = (int)Math.ceil(tmax/T);
		double t = 0;
		IMatrix[] vrijednosti = new IMatrix[brojIteracija];
		for(int i = 0; i < brojIteracija; i++) {
			vrijednosti[i] = integrator.provediIteraciju();
			t += T;
			integrator.postavi(vrijednosti[i], t);
		}
		return vrijednosti;
	}
}
