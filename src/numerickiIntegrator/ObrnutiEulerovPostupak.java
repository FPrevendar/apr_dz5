package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class ObrnutiEulerovPostupak implements INumerickiIntegrator, IntegratorKorektor{
	
	private IMatrix A;
	private IMatrix B;
	private IMatrix xk;
	private double T;
	private RtFunkcija rt;
	private double t;
	
	public ObrnutiEulerovPostupak(double T, IMatrix A, IMatrix B, IMatrix xk, RtFunkcija rt) {
		this.A = A;
		this.B = B;
		this.T = T;
		this.rt = rt;
		this.xk = xk;
		this.t = 0;
	}
	
	@Override
	public IMatrix provediIteraciju() {
		IMatrix uMinusTAInverz = Matrix.identity(xk.visina()).sub(A.copy().mul(T)).inverz();
		IMatrix m = uMinusTAInverz.mul(xk);
		m.add(uMinusTAInverz.copy().mul(T).mul(B).mul(rt.evaluate(t + T)));
		return m;
	}
	
	@Override
	public void postavi(IMatrix xk, double t) {
		this.xk = xk;
		this.t = t;
	}

	@Override
	public IMatrix korigiraj(IMatrix predikcija) {
		IMatrix xkPlus1 = xk.add(A.mul(predikcija).add(B.mul(rt.evaluate(t + T))).mul(T));
		return xkPlus1;
	}
}