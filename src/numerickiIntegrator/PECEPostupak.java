package numerickiIntegrator;

import matrix.IMatrix;

@SuppressWarnings("unused")
public class PECEPostupak implements INumerickiIntegrator {
	
	private IMatrix A;
	private IMatrix B;
	private IMatrix xk;
	private double T;
	private RtFunkcija rt;
	private double t;
	private IntegratorPrediktor prediktor;
	private IntegratorKorektor korektor;
	private int brojKorekcija;
	
	public PECEPostupak(IntegratorPrediktor pred, IntegratorKorektor kore, int brojPonavljanja, double T, IMatrix A, IMatrix B, IMatrix xk, RtFunkcija rt) {
		this.A = A;
		this.B = B;
		this.T = T;
		this.rt = rt;
		this.xk = xk;
		this.t = 0;
		this.prediktor = pred;
		this.korektor = kore;
		this.brojKorekcija = brojPonavljanja;
	}

	@Override
	public IMatrix provediIteraciju() {
		prediktor.postavi(xk, t);
		IMatrix predikcija = prediktor.predvidi();
		korektor.postavi(xk, t);
		for(int i = 0; i < brojKorekcija; i++) {
			predikcija = korektor.korigiraj(predikcija);
		}
		return predikcija;
	}

	@Override
	public void postavi(IMatrix xk, double t) {
		this.xk = xk;
		this.t = t;
	}

}
