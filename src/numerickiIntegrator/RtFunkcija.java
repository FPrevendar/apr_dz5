package numerickiIntegrator;

import matrix.IMatrix;

public interface RtFunkcija {
	public IMatrix evaluate(double t);
}
