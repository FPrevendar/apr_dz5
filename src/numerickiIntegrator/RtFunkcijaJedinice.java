package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class RtFunkcijaJedinice implements RtFunkcija{
	
	private int dimenzija;
	private IMatrix m;
	
	public RtFunkcijaJedinice(int d) {
		dimenzija = d;
		m = new Matrix(dimenzija, 1);
		for(int i = 0; i < dimenzija; i++) {
			m.set(i, 0, 1);
		}
	}
	
	@Override
	public IMatrix evaluate(double t) {
		return m.copy();
	}
	
}
