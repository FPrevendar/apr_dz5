package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class RtFunkcijaKonstanta implements RtFunkcija{
	
	private int dimenzija;
	private IMatrix m;
	private double konstanta;
	
	public RtFunkcijaKonstanta(int d, double konstanta) {
		dimenzija = d;
		m = new Matrix(dimenzija, 1);
		this.konstanta = konstanta;
		for(int i = 0; i < dimenzija; i++) {
			m.set(i, 0, this.konstanta);
		}
	}
	
	@Override
	public IMatrix evaluate(double t) {
		return m.copy();
	}
	
}
