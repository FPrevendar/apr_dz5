package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class RtFunkcijaLinearna implements RtFunkcija{
	
	private int dimenzija;
	private IMatrix A;
	private IMatrix B;
	
	public RtFunkcijaLinearna(int d, IMatrix a, IMatrix b) {
		dimenzija = d;
		this.A = new Matrix(dimenzija, 1);
		this.B = new Matrix(dimenzija, 1);
		for(int i = 0; i < dimenzija; i++) {
			this.A.set(i, 0, a.get(i, 0));
			this.B.set(i, 0, b.get(i, 0));
		}
	}
	
	@Override
	public IMatrix evaluate(double t) {
		return A.copy().mul(t).add(B);
	}
	
}
