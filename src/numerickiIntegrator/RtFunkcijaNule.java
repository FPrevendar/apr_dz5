package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class RtFunkcijaNule implements RtFunkcija {
	
	private int dimenzija;
	
	public RtFunkcijaNule(int d) {
		dimenzija = d;
	}

	@Override
	public IMatrix evaluate(double t) {
		return new Matrix(dimenzija, 1);
	}

}
