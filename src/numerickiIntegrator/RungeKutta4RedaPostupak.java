package numerickiIntegrator;

import matrix.IMatrix;

public class RungeKutta4RedaPostupak implements INumerickiIntegrator, IntegratorPrediktor {
	
	private IMatrix A;
	private IMatrix B;
	private IMatrix xk;
	private double T;
	private RtFunkcija rt;
	private double t;
	
	public RungeKutta4RedaPostupak(double T, IMatrix A, IMatrix B, IMatrix xk, RtFunkcija rt) {
		this.A = A;
		this.B = B;
		this.T = T;
		this.rt = rt;
		this.xk = xk;
		this.t = 0;
	}

	@Override
	public IMatrix provediIteraciju() {
		IMatrix m1 = A.mul(xk).add(B.mul(rt.evaluate(t)));
		IMatrix m2 = A.mul(xk.add(m1.copy().mul(T/2))).add(B.mul(rt.evaluate(t + T/2)));
		IMatrix m3 = A.mul(xk.add(m2.copy().mul(T/2))).add(B.mul(rt.evaluate(t + T/2)));
		IMatrix m4 = A.mul(xk.add(m3.copy().mul(T))).add(B.mul(rt.evaluate(t + T)));
		IMatrix xkPlus1 = xk.add(m1.add(m2.copy().mul(2)).add(m3.copy().mul(2)).add(m4).mul(T/6));
		return xkPlus1;
	}

	@Override
	public void postavi(IMatrix xk, double t) {
		this.xk = xk;
		this.t = t;
	}

	@Override
	public IMatrix predvidi() {
		return provediIteraciju();
	}

}
