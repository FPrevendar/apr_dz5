package numerickiIntegrator;

import matrix.IMatrix;
import matrix.Matrix;

public class TrapezniPostupak implements INumerickiIntegrator, IntegratorKorektor {
	
	private IMatrix A;
	private IMatrix B;
	private IMatrix xk;
	private double T;
	private RtFunkcija rt;
	private double t;
	
	public TrapezniPostupak(double T, IMatrix A, IMatrix B, IMatrix xk, RtFunkcija rt) {
		this.A = A;
		this.B = B;
		this.T = T;
		this.rt = rt;
		this.xk = xk;
		this.t = 0;
	}

	@Override
	public IMatrix provediIteraciju() {
		IMatrix IMinusTPolaAInverz = Matrix.identity(xk.visina()).sub(A.copy().mul(T/2)).inverz();
		IMatrix IPlusTPolaA = Matrix.identity(xk.visina()).add(A.copy().mul(T/2));
		IMatrix m = IMinusTPolaAInverz.mul(IPlusTPolaA).mul(xk);
		m.add(IMinusTPolaAInverz.copy().mul(T/2).mul(B).mul(rt.evaluate(t).add(rt.evaluate(t + T))));
		return m;
	}

	@Override
	public void postavi(IMatrix xk, double t) {
		this.xk = xk;
		this.t = t;
	}

	@Override
	public IMatrix korigiraj(IMatrix predikcija) {
		IMatrix xktocka = A.mul(xk).add(B.mul(rt.evaluate(t)));
		IMatrix xkPlus1Tocka = A.mul(predikcija).add(B.mul(rt.evaluate(t + T)));
		IMatrix xkPlus1 = xk.add(xktocka.add(xkPlus1Tocka).mul(T/2));
		return xkPlus1;
	}
	
	

}
