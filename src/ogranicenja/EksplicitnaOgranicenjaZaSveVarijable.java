package ogranicenja;

import point.IPoint;

public class EksplicitnaOgranicenjaZaSveVarijable implements IOgranicenje {
	
	private IPoint min;
	private IPoint max;
	
	public EksplicitnaOgranicenjaZaSveVarijable(IPoint min, IPoint max) {
		if(min.getDimenstion() != max.getDimenstion()) {
			throw new IllegalArgumentException("Ogranicenja su krivih dimenzija");
		}
		this.min = min.copy();
		this.max = max.copy();
	}

	@Override
	public boolean jeLiZadovoljeno(IPoint tocka) {
		if(!checkDimension(tocka)) {
			throw new IllegalArgumentException("Dimenzije tocke nisu dobre");
		}
		for(int i = 0; i < tocka.getDimenstion(); i++) {
			if(tocka.get(i) < min.get(i) || tocka.get(i) > max.get(i)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean checkDimension(IPoint tocka) {
		if(tocka.getDimenstion() == max.getDimenstion()) {
			return true;
		}
		return false;
	}
	
	public IPoint getMin() {
		return min.copy();
	}
	
	public IPoint getMax() {
		return max.copy();
	}

}
